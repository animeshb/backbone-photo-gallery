App.Collections.Photos = Backbone.Collection.extend({
	model: App.Models.Photo,
	url: '/laravel/first-project/public/photos'
});


App.Collections.PaginatedCollection = Backbone.Paginator.requestPager.extend({
  model: App.Models.Photo,
  paginator_core: {
    dataType: 'json',
    url: '/laravel/first-project/public/photos'
  },

  paginator_ui: {
    firstPage: 1,
    currentPage: 1,
    perPage: 2,
    totalPages: 10
  },

  server_api: {
    'per_page': function() { return this.perPage },
    'page': function() { return this.currentPage },
    'sort': function() {
      if(this.sortField === undefined)
        return 'popular';
      return this.sortField;
    }
  },

  parse: function (response) {
    //$('#products-area').spin(false);
    this.totalRecords = response.total;
    this.totalPages = Math.ceil(response.total / this.perPage);

    return response.data;
  }

});


App.Models.Photo = Backbone.Model.extend({
	validate: function(attrs) {
		if ( ! attrs.title ) {
			return 'Title is required.';
		}

		if ( ! attrs.image ) {
			return 'Image is required.';
		}
	}
});
/*
|--------------------------------------------------------------------------
| Global App View
|--------------------------------------------------------------------------
*/
App.Views.App = Backbone.View.extend({
	initialize: function() {
		//vent.on('photo:edit', this.editContact, this);

		 //this.collection.on('reset', this.renderCats, this);
		
		var allContactsView = new App.Views.Photos({ collection: App.photos }).render();

		//concole.log( allContactsView );
		$('#allContacts').parent().append(allContactsView.el);
	},

	renderCats : function(){

		var allContactsView = new App.Views.Photos({ collection: App.photos });

		//concole.log( allContactsView );
		//$('#allContacts').html(allContactsView.el);
 
	},
	

	editContact: function(contact) {

		console.log( 'in edit contact');
		var editContactView = new App.Views.EditContact({ model: contact });
		$('#editContact').html(editContactView.el);
	}
});

App.Views.PaginatedView = Backbone.View.extend({

  events: {
    'click button.prev': 'gotoPrev',
    'click button.next': 'gotoNext',
    'click a.page': 'gotoPage'
  },

  template: _.template($('#paginationTemplate').html()),

  initialize: function () {
    this.collection.on('reset', this.render, this);
    this.collection.on('sync', this.render, this);
    this.render().$el.appendTo('#pagination');

    //('#pagination').appen


  },

  render: function () {
    var html = this.template(this.collection.info());
    //console.log( html );
    this.$el.html(html);

    return this;
  },

  gotoPrev: function (e) {
    e.preventDefault();
   // $('#products-area').spin();
    this.collection.requestPreviousPage();
  },

  gotoNext: function (e) {
    e.preventDefault();
    //$('#products-area').spin();
    this.collection.requestNextPage();
  },

  gotoPage: function (e) {
    e.preventDefault();
    //$('#products-area').spin();
    var page = $(e.target).text();
    this.collection.goTo(page);
  }

});


/*
|--------------------------------------------------------------------------
| Add Contact View
|--------------------------------------------------------------------------
*/


App.Views.AddContact = Backbone.View.extend({
	el: '#addContact',

	initialize: function() {
		this.title = $('#title');
		this.description = $('#description');
		this.addphoto = $('#addphoto');
	},

	events: {
		'submit': 'addContact',
		"change #addphoto": "changePhoto"
	},

	dropPhoto: function (event) {
        event.stopPropagation();
        event.preventDefault();
         
        var e = event.originalEvent;
        // The DataTransfer object holding the data.
        e.dataTransfer.dropEffect = 'copy';
        this.pictureFile = e.dataTransfer.files[0];
        // Read the image file from the local file system
        // and display it in the img tag.
        var reader = new FileReader();
        reader.onloadend = function () {
            $('#image').attr('src', reader.result);
        };
        reader.readAsDataURL(this.pictureFile);
        return false;
    },

	changePhoto: function(event) {

		event.stopPropagation();
        // To prevent the browser default handling of the data:
        // default is open as link on drop.
        event.preventDefault();
         
        var newFile = event.target.files[0];

        console.log( newFile );
        if( ! newFile.type.match(/image.*/i) ){
            alert('Insert an image!');
        } else {
            this.pictureFile = newFile;
            // Read the image file from the local file system
            // and display it in the img tag.
            var reader = new FileReader();
            reader.onloadend = function () {
                $('#image').attr('src',reader.result);
            };
            reader.readAsDataURL(this.pictureFile);
        }
        return false;
	},

	addContact: function(e) {
		e.preventDefault();

		var self = this;
        if (this.pictureFile) {
            //console.log( this.pictureFile.name);
            //
             
            // append photo into FormData object
            var fileData = new FormData();
            fileData.append('file', this.pictureFile);
             
            // upload FormData object by XMLHttpRequest
            $.ajax({
                url: '/laravel/first-project/public/photos/upload',
                type: 'POST',
                data: fileData,
                processData: false,
                cache: false,
                contentType: false
            })
            .done(function (data) {

				self.collection.create({

					image: data,
					title: self.title.val(),
					description: self.description.val(),
				}, { wait: true });

				self.clearForm();

				return true;

           	
            })
            .fail(function () {
                console.log('Error! An error occurred while uploading '
                    + self.pictureFile.name + ' !' );
                return false;
            });
        }

        return false; 


		
	},

	clearForm: function() {
		this.title.val('');
		this.description.val('');
		this.addphoto.val('');

	}
});



/*
|--------------------------------------------------------------------------
| Edit Contact View
|--------------------------------------------------------------------------
*/


App.Views.EditContact = Backbone.View.extend({
	template: template('editContactTemplate'),

	initialize: function() {
		this.render();

		this.form = this.$('form');
		this.title = this.form.find('#edit_title');
		this.description = this.form.find('#edit_description');
	},

	events: {
		'submit form': 'submit',
		'click button.cancel': 'cancel',
		"change #changephoto": "changePhoto"
	},

	dropPhoto: function (event) {
        event.stopPropagation();
        event.preventDefault();
         
        var e = event.originalEvent;
        // The DataTransfer object holding the data.
        e.dataTransfer.dropEffect = 'copy';
        this.pictureFile = e.dataTransfer.files[0];
        // Read the image file from the local file system
        // and display it in the img tag.
        var reader = new FileReader();
        reader.onloadend = function () {
            $('#catphoto').attr('src', reader.result);
        };
        reader.readAsDataURL(this.pictureFile);
        return false;
    },

	changePhoto: function(event) {

		event.stopPropagation();
        // To prevent the browser default handling of the data:
        // default is open as link on drop.
        event.preventDefault();
         
        var newFile = event.target.files[0];

        console.log( newFile );
        if( ! newFile.type.match(/image.*/i) ){
            alert('Insert an image!');
        } else {
            this.pictureFile = newFile;
            // Read the image file from the local file system
            // and display it in the img tag.
            var reader = new FileReader();
            reader.onloadend = function () {
                $('#catphoto').attr('src', reader.result);
            };
            reader.readAsDataURL(this.pictureFile);
        }
        return false;
	},


	submit: function(e) {
		e.preventDefault();

		var self = this;
        if (this.pictureFile) {
            //console.log( this.pictureFile.name);
            //
             
            // append photo into FormData object
            var fileData = new FormData();
            fileData.append('file', this.pictureFile);
             
            // upload FormData object by XMLHttpRequest
            $.ajax({
                url: '/laravel/first-project/public/photos/upload',
                type: 'POST',
                data: fileData,
                processData: false,
                cache: false,
                contentType: false
            })
            .done(function (data) {

            	self.model.set("image", data);

            	self.model.save({
					title: self.title.val(),
					description: self.description.val(),
				});

           	
            })
            .fail(function () {
                console.log('Error! An error occurred while uploading '
                    + self.pictureFile.name + ' !' );
                return false;
            });
        } else {

        		this.model.save({
					title: this.title.val(),
					description: this.description.val(),
				});

        }

		

		this.remove();
	},

	cancel: function() {
		this.remove();
	},

	render: function() {
		var html = this.template( this.model.toJSON() );

		this.$el.html(html);
		
		return this;
	}
});




/*
|--------------------------------------------------------------------------
| All Contacts View
|--------------------------------------------------------------------------
*/
App.Views.Photos = Backbone.View.extend({
	tagName: 'tbody',

	initialize: function() {
		this.collection.on('sync', this.render, this);
		
	},

	render: function() {
		$('#allContacts').next().remove();

		this.collection.each( this.addOne, this );

		$('#allContacts').parent().append(this.$el);

		return this;
	},

	addOne: function(photo) {

		var contactView = new App.Views.Photo({ model: photo });

		this.$el.append(contactView.render().el);
	}
});


/*
|--------------------------------------------------------------------------
| Single Contact View
|--------------------------------------------------------------------------
*/
App.Views.Photo = Backbone.View.extend({
	tagName: 'tr',
	//className:'edit',

	template: template('allContactsTemplate'),

	initialize: function() {
		this.model.on('destroy', this.unrender, this);
		this.model.on('change', this.render, this);
	},

	events: {
		'click a.delete': 'deleteContact',
		'click a.edit'  : 'editContact'
		
		//,'click title'  : 'editContact'
	},


	editContact: function() {

		vent.trigger('photo:edit', this.model);
	},

	deleteContact: function() {
		this.model.destroy();
	},

	render: function() {
		this.$el.html( this.template( this.model.toJSON() ) );
		return this;
	},

	unrender: function() {
		this.remove();
	}
});
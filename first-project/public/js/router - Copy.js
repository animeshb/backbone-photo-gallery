App.Router = Backbone.Router.extend({
	routes: {
		'': 'index',

		 //"":                "list",
        "photos/new":        "newPhoto",
        "photos/:id":        "teaDetails"
	},

	index: function() {
			App.photos = new App.Collections.PaginatedCollection();

			App.photos.fetch().then(function() {

				new App.Views.App({ collection: App.photos });
				new App.Views.PaginatedView({collection:App.photos});

			});
	}

});

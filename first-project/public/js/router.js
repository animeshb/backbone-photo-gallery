App.Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		//'#': 'index',

		 //"":                "list",
        "photos/new":        "newPhoto",
        "photos/:id/edit":        "ptDetails"
	},

	index: function() {

		//console.log( 'sssss' );

		if(!App.photos){
			App.photos = new App.Collections.PaginatedCollection();

			App.photos.fetch().then(function() {

				 new App.Views.App({ collection: App.photos });
			});


		 } else {

		   new App.Views.App({ collection: App.photos });
		 }



	},

	newPhoto:function(){

		if(!App.photos){

			App.photos = new App.Collections.PaginatedCollection();

			App.photos.fetch().then(function() {
				

				var addContactView = new App.Views.AddContact({ collection: App.photos });
				//$('#content').html('');
				$('#content').html(addContactView.render().el);

			});


		} else {

			var addContactView = new App.Views.AddContact({ collection: App.photos });
			//$('#content').html('');
			$('#content').html(addContactView.render().el);
		}

	},


	ptDetails: function(id){
		

		if(!App.photos){

			App.photos = new App.Collections.PaginatedCollection();

			App.photos.fetch().then(function() {
				var photo = App.photos.get(id);

				console.log(photo);

		        var editContactView = new App.Views.EditContact({ model: photo });
				$('#content').html(editContactView.render().el);

			});


		}else{

			var photo = App.photos.get(id);
	        var editContactView = new App.Views.EditContact({ model: photo });
			$('#content').html(editContactView.render().el);
		}

		

	}


});

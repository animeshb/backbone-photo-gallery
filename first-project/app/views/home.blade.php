<!doctype html>
<html>
<head>
	<title></title>
	<style>
	table thead td { font-weight: bold; }
	.module { margin-bottom: 2em; }
	</style>
</head>
<body>

<h1>Contacts</h1>

<script type="text/html" id="addContactTemplate">
<form id="addContact" class="module">
	<div>
		<label for="title">Title: </label>
		<input type="text" id="addtitle" name="addtitle">
	</div>

	
	<div>
		<label for="description">Description:</label>
		<textarea id="adddescription" name="adddescription"></textarea>
	</div>
	
	
	<div>
		<label for="changephoto">Photo Upload:</label>
		<input type="file" name="addphoto" id="addphoto">
	</div>

	<div>
		<img src="" alt="" width="100px" width="100px" name="image" id="image">
	</div>

	<div>
		<input type="submit" value="Add Contact">
	</div>
</form>
</script>


 <div class="text-center" id="pagination"></div>


<div id="content"></div>

<script type="text/html" id="paginationTemplate">
<a href="#photos/new">Add Photo</a>
 <% if (currentPage != 1) { %>
   <button class="prev btn-default btn">Prev</button>
 <% } %>
 <div class="btn-group">
   <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Page <%= currentPage %> <span class="caret"></span></button>
   <ul class="dropdown-menu">
     <% for(p=1;p<=totalPages;p++){ %>
       <% if (currentPage == p) { %>
         <li class="active"><a href="#" class="page"><%= p %></a></li>
       <% } else { %>
         <li><a href="#" class="page"><%= p %></a></li>
       <% } %>
     <% }%>
   </ul>
 </div>
 <% if (lastPage != currentPage && lastPage != 0) { %>
  <button class="btn btn-default next">Next</button>
 <% } %>
</script>

<script id="allContactsTemplate" type="text/template">

	<p><strong><%= id %></strong></p>
	<p><strong><%= title %></strong></p>
	<p><img src="<%= image %>" alt="" width="100px" width="100px"></p>
	<p><a href="#photos/<%= id %>/edit" class="edit">Edit</a></p>
	
</script>

<script id="editContactTemplate" type="text/template">
	<h2>Edit Photo: <%= title %></h2>
	<form id="editContact">
		<div>
			<label for="edit_title">Title: </label>
			<input type="text" id="edit_title" name="edit_title" value="<%= title %>">
		</div>

		<div>
			<img id='catphoto' src="<%= image %>" alt="" width="100px" width="100px">
		</div>

		<div>
			<label for="changephoto">Photo Upload:</label>
			<input type="file" name="changephoto" id="changephoto">
		</div>
	
		
		<div>
			<label for="edit_description">Description:</label>
			<textarea id="edit_description" name="edit_description"><%= description %></textarea>
		</div>
		<div>
			<input type="submit" value="Add Contact">
			<button type="button" class="cancel">Cancel</button>
		</div>
		<div>
			<button type="button" class="delete">Delete</button>
		</div>
	</form>
</script>

<script src="js/jquery.js"></script>
<script src="js/underscore.js"></script>
<script src="js/backbone.js"></script>
<script src="js/backbone.paginator.js"></script>
<script src="js/main.js"></script>
<script src="js/models.js"></script>
<script src="js/collections.js"></script>
<script src="js/views.js"></script>
<script src="js/router.js"></script>

<script>
	App.router = new App.Router;
	Backbone.history.start();

	
</script>

</body>
</html>
<!doctype html>
<html>
<head>
	<title></title>
	<style>
	table thead td { font-weight: bold; }
	.module { margin-bottom: 2em; }
	</style>
</head>
<body>

<h1>Contacts</h1>

<form id="addContact" class="module">
	<div>
		<label for="title">Title: </label>
		<input type="text" id="title" name="title">
	</div>

	
	<div>
		<label for="description">Description:</label>
		<textarea id="description" name="description"></textarea>
	</div>
	
	
	<div>
		<label for="changephoto">Photo Upload:</label>
		<input type="file" name="addphoto" id="addphoto">
	</div>

	<div>
		<img src="" alt="" width="100px" width="100px" name="image" id="image">
	</div>
	 


	<div>
		<input type="submit" value="Add Contact">
	</div>
</form>

<table class="module">
	<thead id="allContacts">
		<tr>
			<th>Id</th>
			<th>Title</th>
			<th>Image</th>
		</tr>
	</thead>
</table>

 <div class="text-center" id="pagination"></div>

<div id="editContact" class="module">

</div>


<div id="content"></div>

<script type="text/html" id="paginationTemplate">
 <% if (currentPage != 1) { %>
   <button class="prev btn-default btn">Prev</button>
 <% } %>
 <div class="btn-group">
   <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Page <%= currentPage %> <span class="caret"></span></button>
   <ul class="dropdown-menu">
     <% for(p=1;p<=totalPages;p++){ %>
       <% if (currentPage == p) { %>
         <li class="active"><a href="#" class="page"><%= p %></a></li>
       <% } else { %>
         <li><a href="#" class="page"><%= p %></a></li>
       <% } %>
     <% }%>
   </ul>
 </div>
 <% if (lastPage != currentPage && lastPage != 0) { %>
  <button class="btn btn-default next">Next</button>
 <% } %>
</script>

<script id="allContactsTemplate" type="text/template">

	<td><strong><%= id %></strong></td>
	<td><strong><%= title %></strong></td>
	<td><img src="<%= image %>" alt="" width="100px" width="100px"></td>
	<td><a href="#photos/<%= id %>/edit" class="edit">Edit</a></td>
	<td><a href="#photos/<%= id %>" class="delete">Delete</a></td>
	
</script>

<script id="editContactTemplate" type="text/template">
	<h2>Edit Photo: <%= title %></h2>
	<form id="editContact">
		<div>
			<label for="edit_title">Title: </label>
			<input type="text" id="edit_title" name="edit_title" value="<%= title %>">
		</div>

		<div>
			<img id='catphoto' src="<%= image %>" alt="" width="100px" width="100px">
		</div>

		<div>
			<label for="changephoto">Photo Upload:</label>
			<input type="file" name="changephoto" id="changephoto">
		</div>
	
		
		<div>
			<label for="edit_description">Description:</label>
			<textarea id="edit_description" name="edit_description"><%= description %></textarea>
		</div>
		<div>
			<input type="submit" value="Add Contact">
			<button type="button" class="cancel">Cancel</button>
		</div>
	</form>
</script>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="http://underscorejs.org/underscore.js"></script>
<script src="http://backbonejs.org/backbone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.paginator/0.8/backbone.paginator.min.js"></script>
<script src="js/main.js"></script>
<script src="js/models.js"></script>
<script src="js/collections.js"></script>
<script src="js/views.js"></script>
<script src="js/router.js"></script>

<script>
	new App.Router;
	Backbone.history.start();

	
</script>

</body>
</html>
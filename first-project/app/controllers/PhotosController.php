<?php

class PhotosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /photos
	 *
	 * @return Response
	 */
	public function index()
	{
		//

			/*
		
					// Get the number of items to show per page (defaults to 6 items)
			    $perPage = e(Input::get('per_page','2'));

			    // Get the index of the current page (defaults to the first page)
			    $page = e(Input::get('page','1'));

			    // Get the sorting parameter (defaults to popular)
			    $sort = e(Input::get('sort','popular'));

			    // Calculate the offset of the items
			    $offset = $page*$perPage-$perPage;

			    // The count of items will be updated from the query
			    $count = 0;

			    */

			    $photos = Photo::all();

			    $count = $photos->count();

			    // Retrieve the products using Laravel's Eloquent ORM methods
			    //$products = $photos->take($perPage)->offset($offset)->get();

			    //$products =  Photo::paginate(2);
			    $products = DB::table('photos')->orderBy('id', 'desc')->paginate(2);
			    //Return the results as JSON data
			    return Response::json(array(
			      'data'=>$products->toArray()['data'],
			      'total' => $count
			    ));




			    //return Photo::all();

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /photos/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /photos
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::json();

		return Photo::create(array(
			'title' => Input::get('title'),
			'image' => Input::get('image'),
			'description' => Input::get('description')
		));
	}

	/**
	 * Display the specified resource.
	 * GET /photos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /photos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//

		return Photo::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /photos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$photo = Photo::find($id);
		$input = Input::json();
		
		$photo->title = Input::get('title');
		$photo->description = Input::get('description');
		$photo->image = Input::get('image');
		$photo->save();

	}


	public function upload(){


		if (Input::hasFile('file')) {

	        $file            = Input::file('file');
	        $destinationPath = public_path().'/image/';
	        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
	        $uploadSuccess   = $file->move($destinationPath, $filename);
    	}

    	echo '/laravel/first-project/public/image/'.$filename;
    	exit;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /photos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		Photo::find($id)->delete();
	}

}
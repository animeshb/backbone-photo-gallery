<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PhotosTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Photo::create([

				'title' => $faker->sentence(),
                'description' => $faker->realtext(1000),
                'image' => $faker->imageUrl( $width = 640, $height = 480, 'cats')     // 'http://lorempixel.com/800/600/cats/'

			]);
		}
	}

}